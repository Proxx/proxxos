# ProxxOS#

ProxxOS is designed to help Windows administrators with support operations in the field
### What is this ProxxOS for? ###

* [GParted (GNOME Partition Editor)](http://gparted.org/)
    - ntfs ext exfat fat
* [Clonezilla (Software for Disk Imaging and Cloning)](http://clonezilla.org/)
* [TestDisk - Partition Recovery and File Undelete](http://www.cgsecurity.org/wiki/TestDisk)
* [chntpw (Offline NT Password & Registry Editor)](http://www.chntpw.com/)
* [WireShark](https://www.wireshark.org/)
* [EtherApe](http://etherape.sourceforge.net/)
* [RDesktop] (http://www.rdesktop.org/)
* Mount all disks
* Samba share all drives

### How do I get set up? ###

* Summary of set up
* Configuration * WIP
* Dependencies * WIP
* How to run tests * WIP
* Deployment instructions * WIP


### Who do I talk to? ###

* Proxx (Marco van G.)