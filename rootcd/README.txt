ProxxOS
========


ProxxOS distro
------------------

    Features
     - Chntpw Offline password & registry editor
     - etherape network discovery
     - Gparted partition editor
     - Testdisk & PhotoRec
     - Memtest 86+
     - Samba
     - Remote desktop

    Scripts
     - Mount all disks
        - populate fstab on boot
        - mount -a
     - Share all disks
        - for loop /mnt and generate samba.conf
        - reload samba

Snappy Driver Installer
-----------------------

    Features
     - Install windows drivers

NirSoft Tools
-------------

    Features
     - Password Tools
     - Product key recovery
     - Outlook NK2 editor
