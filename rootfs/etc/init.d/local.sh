#!/bin/sh
#
# /etc/init.d/local.sh: Local startup commands
#
# All commands here will be executed at boot time.
#

mntall=0        # Mount all disks
smball=0        # samba share all disks
chntpw=0        # Boot chntpw dialogs
sshd=0          # enable sshd
clonezilla=0
all=0

grep -q samba /proc/cmdline && smball=1
grep -q chntpw /proc/cmdline && chntpw=1
grep -q sshd /proc/cmdline && sshd=1
grep -q clonezilla /proc/cmdline && clonezilla=1
grep -q all /proc/cmdline && all=1

if [ $all == 1 ]
then
    mntall=1
    smball=1
    chntpw=1
    sshd=1
fi

if [ $mntall == 1 ] || [ $smball == 1 ]
then
    /bin/mountall
    echo boot mountall >> /tmp/bootopts
fi

if [ $smball == 1 ]
then
    /bin/fileserver
    echo boot shareall >> /tmp/bootopts
fi

if [ $chntpw == 1 ]
then
    /bin/changepw
    echo boot chntpw >> /tmp/bootopts
fi

if [ $sshd == 1 ]
then
    /etc/init.d/dropbear start
    echo boot dropbear >> /tmp/bootopts
fi

tuxpwd=`cat /etc/shadow | grep tux | cut -f2 -d:`
if [ "$tuxpwd" == "" ]
then
    echo set password to tux > /tmp/bootopts
    echo tux:tux | chpasswd
fi

if [ $clonezilla == 1 ]
then
    /bin/clonezilla
    echo boot clonezilla >> /tmp/bootopts
fi

