$Server = (Get-Content -Raw buildserver).trim()

$Path = "$PSScriptRoot\..\iso\"
Resolve-Path $PSScriptRoot\..\iso\*.iso | Get-Item | ForEach-Object { $_ | Rename-Item -NewName $("{0}_{1}" -f $_.BaseName, [datetime]::Now.ToString("yyyyMMdd_HHmmss")) -Verbose }
Copy-Item -Path \\$Server\root\home\slitaz\5.0\distro\*.iso -Destination $Path -Verbose