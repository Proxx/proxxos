
#!/bin/sh
#
# ProxxOS Init script
#

# Variables

BUILD_LOG="/tmp/build.log" # build logfile location
SLITAZ_HOME="/home/slitaz" # SliTaz directory.
SLITAZ_VERSION=$(cat /etc/slitaz-release) # SliTaz version to use for ISO name and files path.
WORK_DIR="$SLITAZ_HOME/$SLITAZ_VERSION" # Tazlito work directory with flavors, distro and packages files.
DISTRO="$WORK_DIR/distro" # Path to the distro tree to gen-distro from a list of packages.
ADDFILES="$DISTRO/addfiles" # Path to the directory containing additional files
SAMBA_CONF="/etc/samba/smb.conf"

# ProxxOS packagelist
echo "
chntpw
clonezilla
etherape
exfat-utils
fuse-exfat
git
ntfsprogs
rdesktop
samba
tazlito
wireshark
" > /tmp/init.package.list

# reset all switches to default values
GIT_CLONE="false"; GEN_DISTRO="false"; SAMBA_SHARE="false";

while getopts "gesh" OPTION; do
        case $OPTION in

                g)
                        GIT_CLONE="true"
                        ;;
                e)
                        GEN_DISTRO="true"
                        ;;
                s)
                        SAMBA_SHARE="true"
                        ;;
                h)
                        echo "Usage:"
                        echo "args.sh -h "
                        echo "args.sh -git "
                        echo ""
                        echo "   -g     clone repository."
                        echo "   -e     generate ProxxOS distro"
                        echo "   -h     help (this helptext)"
                        exit 0
                        ;;

        esac
done

if [ $# -eq 0 ]
then
    GIT_CLONE="true"
    GEN_DISTRO="true"
    SAMBA_SHARE="true"
fi

# set tazpkg mirror to the default
tazpkg -sm http://download.tuxfamily.org/slitaz//packages/5.0/

# recharge the package list
tazpkg recharge 

# install packagelist
yes | tazpkg get-install-list /tmp/init.package.list

# update all installed packages
yes | tazpkg up

# if build is local initialize variables and clone repository
if [[ ! -z $BITBUCKET_CLONE_DIR ]] 
then
    BITBUCKET_CLONE_DIR="/tmp/ProxxOS"
    if [ -d "$BITBUCKET_CLONE_DIR" ]
    then
        rm -R "$BITBUCKET_CLONE_DIR"
    fi
    git config --global http.sslverify "false" # skip ssl verification of git
    git clone https://Proxx@bitbucket.org/Proxx/proxxos.git "$BITBUCKET_CLONE_DIR" # clone repo to BITBUCKET_CLONE_DIR variable
fi

if ! [ -d "$SLITAZ_HOME" ]
then
    mkdir "$SLITAZ_HOME"
    mkdir "$WORK_DIR"
    mkdir "$DISTRO"
fi

if ! [ -d "$ADDFILES" ]
then
    mkdir "$ADDFILES"
else
    rm -R "$ADDFILES/rootfs"
    rm -R "$ADDFILES/rootcd"
fi

# copy rootfs and rootcd to distro dir
cp -R "$BITBUCKET_CLONE_DIR/rootfs" "$ADDFILES"
cp -R "$BITBUCKET_CLONE_DIR/rootcd" "$ADDFILES"

# set file permissions
chmod +x "$ADDFILES/rootfs/bin/*"               # binaries
chmod +x "$ADDFILES/rootfs/etc/init.d/local.sh" # boot script
chmod +x "$ADDFILES/rootfs/scripts/*"           # chntpw script folder

# enable samba file sharing
if [ "$GEN_DISTRO" == "true" ]
then
    echo "
[global]
encrypt passwords = Yes
load printers = No
map to guest = Bad password
max protocol = SMB2
netbios name = prxos
security = user
server string = ProxxOS Recovery and tools (slitaz)
socket options = TCP_NODELAY SO_RCVBUF=131072 SO_SNDBUF=131072
workgroup = WORKGROUP

[root]
force group = root
force user = root
guest ok = Yes
path = /
read only = Yes
writable = Yes

" > "$SAMBA_CONF";

    /etc/init.d/samba restart
fi

if [ "$GEN_DISTRO" == "true" ]
then
    if [ -d "$ADDFILES/rootfs" ]
    then
        tazlito clean-distro
    fi
    ls /var/lib/tazpkg/installed |
        grep -v cdparanoia-III |
        grep -v flac |
        grep -v galculator |
        grep -v git |
        grep -v jasper |
        grep -v mhwaveedit |
        grep -v mtpaint |
        grep -v nanochess |
        grep -v slitaz-doc |
        grep -v speex |
        grep -v sudoku |
        grep -v tazbug |
        grep -v tazinst |
        grep -v tazlito |
        grep -v tazusb | 
        grep -v tazwikiss | 
        grep -v virtualbox-ose-guestutils \
    > /tmp/proxxos-packages.list

    # generate distro
    no | tazlito gen-distro /tmp/proxxos-packages.list
fi
